# AWSCreateAlarm



## Setting up a CPU usage alarm using the AWS Management Console

Use these steps to use the AWS Management Console to create a CPU usage alarm.

To create an alarm based on CPU usage: 
***

### 1. Open the CloudWatch console at [https://console.aws.amazon.com/cloudwatch/](https://console.aws.amazon.com/cloudwatch/)
---
<kbd><img src="Screenshot1.jpg" width="900" border="5px solid"></kbd>

---

### 2. In the navigation pane, choose Alarms, All Alarms.
---
<kbd><img src="Screenshot2.jpg" width="900" border="5px solid"></kbd>

---

### 3. Choose Create alarm. <img src="Screenshot3.jpg" width="100" border="5px solid">
---

### 4. Choose Select metric.
---
<kbd><img src="Screenshot4.jpg" width="900" border="5px solid"></kbd>

---

### 5. In the Browse tab, choose EC2 metrics.
---

<kbd><img src="Screenshot5.jpg" width="900" border="5px solid"></kbd>

---

### 6. Choose a metric category (for example, Per-Instance Metrics).
---
<kbd><img src="Screenshot6.jpg" width="900" border="5px solid"></kbd>

---

### 7. Find the row with the instance that you want listed in the InstanceId column and CPUUtilization in the Metric Name column. Select the check box next to this row, and choose Select metric.
---
<kbd><img src="Screenshot7.jpg" width="900"></kbd>

---

### 8. Next, configure the conditions for the alarm. Set the threshold values for CPU load that will trigger the alarm. Under Specify metric and conditions, for Statistic choose Average, choose one of the predefined percentiles, or specify a custom percentile.
---
Values to use:
- Metric name: CPUUtilization
- Statistics: Average
- Period: 5 Minutes
---
<kbd><img src="Screenshot8.jpg" width="900" border="5px solid"></kbd>

---

### 9. Under Conditions, specify the following:
---
- For Threshold type, choose Static.
- For Whenever CPUUtilization is, specify Greater. Under than..., specify the threshold that is to trigger the alarm to go to ALARM state if the CPU utilization exceeds this percentage. For example, 80.
---
Choose Next.
---

<kbd><img src="Screenshot9.jpg" width="900" border="5px solid"></kbd>

---

### 10. Under Notification, choose Remove to have the alarm not send notifications.
---
<kbd><img src="Screenshot10.jpg" width="900" border="5px solid"></kbd>

---

### 11. Now we could configure some other actions that will trigger the moment our alarm goes off, but for now we only need the alarm so just click Next.
---
<kbd><img src="Screenshot11.jpg" width="900" border="5px solid"></kbd>
---

### 12. Provide a name and optional description for your alarm. Then choose Next.
---
<kbd><img src="Screenshot12.jpg" width="900" border="5px solid"></kbd>

---

### 13. Under Preview and create, confirm that the information and conditions are what you want, then choose Create alarm.
---
<kbd><img src="Screenshot13.jpg" width="900" border="5px solid"></kbd>
<kbd><img src="Screenshot14.jpg" width="900" border="5px solid"></kbd>
<kbd><img src="Screenshot15.jpg" width="900" border="5px solid"></kbd>

---

### 14. After creating the alarm, you will be redirected to the Alarms page. Here, you can monitor the status of your alarms. The alarm will automatically monitor the CPU load based on the threshold you set, and if the condition is met, it will trigger the specified actions.
---
<kbd><img src="Screenshot16.jpg" width="900" border="5px solid"></kbd>

---
That's it! You have successfully created a CloudWatch Alarm using CPU load.

